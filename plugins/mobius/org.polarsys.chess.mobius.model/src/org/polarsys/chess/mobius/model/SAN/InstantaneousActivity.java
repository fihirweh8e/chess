/**
 */
package org.polarsys.chess.mobius.model.SAN;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instantaneous Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.mobius.model.SAN.SANModelPackage#getInstantaneousActivity()
 * @model
 * @generated
 */
public interface InstantaneousActivity extends Activity {
} // InstantaneousActivity
