/*******************************************************************************
 *                  CHESS core plugin
 *
 *               Copyright (C) 2011-2016
 *            Mälardalen University, Sweden
 *
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License
 *  v1.0 which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v20.html
 *******************************************************************************/

modeltype chess uses 'http://CHESS';
modeltype uml uses 'http://www.eclipse.org/uml2/5.0.0/UML';


library CommonUtilities;

query Model::getView(viewName : String) : Package {
	var seq := self.packagedElement[Package]-> union(self.packagedElement[Package].packagedElement[Package]);
	return seq -> selectOne(isStereotyped("CHESS::Core::CHESSViews::" + viewName));
}

query Element::isStereotyped(stereoName : String) : Boolean {
	return self.getAppliedStereotype(stereoName) <> null;
}

query Model::findElementByQualifiedName(in qName : String) : NamedElement {
	var pathParts = qName.tokenize("::")->asOrderedSet();
	if (pathParts->size() = 1) {
		return self;
	};
	var i : Integer = 2;
	var p : Package := self;
	while (i < pathParts->size()) {
		var packageName = pathParts->at(i); 
		p := p.ownedElement[Package]->selectOne(name = pathParts->at(i));
		i := i + 1;
	};
	return p.ownedMember[NamedElement]->selectOne(name = pathParts->last());
}