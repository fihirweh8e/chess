/*
-----------------------------------------------------------------------
--          			CHESS validator plugin					     --
--                                                                   --
--                    Copyright (C) 2011-2012                        --
--                 University of Padova, ITALY                       --
--                                                                   --
-- Author: Alessandro Zovi         azovi@math.unipd.it 		         --
--                                                                   --
-- All rights reserved. This program and the accompanying materials  --
-- are made available under the terms of the Eclipse Public License  --
-- v1.0 which accompanies this distribution, and is available at     --
-- http://www.eclipse.org/legal/epl-v20.html                         --
-----------------------------------------------------------------------
*/

package org.polarsys.chess.validator.managers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.INodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramGraphicalViewer;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.core.util.EMFCoreUtil;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.ClientServerKind;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.ClientServerPort;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.FlowPort;
import org.eclipse.papyrus.editor.PapyrusMultiDiagramEditor;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.uml.diagram.clazz.edit.parts.ComponentOperationCompartmentEditPart;
import org.eclipse.papyrus.uml.diagram.composite.edit.parts.ComponentCompositeCompartmentEditPart;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.internal.impl.DependencyImpl;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.infra.ui.util.EditorUtils;
import org.polarsys.chess.core.profiles.MARTEProfileManager;
import org.polarsys.chess.core.util.CHESSProjectSupport;
import org.polarsys.chess.core.util.uml.ResourceUtils;
import org.polarsys.chess.core.util.uml.UMLUtils;
import org.polarsys.chess.core.views.ViewUtils;
import org.polarsys.chess.service.gui.utils.CHESSEditorUtils;


// TODO: Auto-generated Javadoc
/**
 * The Class AddDiagramElement contains Commands to create new elements in the model
 *  and adds them to the current diagram, if possible
 */
@SuppressWarnings("restriction")
public class AddDiagramElement {
	
	/**
	 * Creates an interface realization from a Component to the Interface provided by its Port
	 *
	 * @param domain the TransactionalEditingDomain of the model
	 * @param comp the Component owner of the port
	 * @param port the ClientServerPort providing and Interface
	 * @return the command
	 */
	public static Command createDependencies(TransactionalEditingDomain domain, final Component comp, final ClientServerPort port){
		return new RecordingCommand(domain) {
			
			@Override
			protected void doExecute() {
				//Dependency dependency = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createDependency();
				//dependency.getClients().add(namedElement1);
				//dependency.getSuppliers().add(namedElement2);
				if (port.getKind() == ClientServerKind.PROVIDED || port.getKind() == ClientServerKind.PROREQ) {
					for (Interface mInterface : port.getProvInterface()) {
						if (!exists(comp, mInterface, true)){
							StringBuilder s = new StringBuilder(25);
							s.append(comp.getName()).append("_Rlz_").append(mInterface.getName());
							comp.createInterfaceRealization(s.toString(), mInterface);
						}						
						//createGraphicalDependency(comp, dep);
					}
				}
				
				if (port.getKind() == ClientServerKind.REQUIRED || port.getKind() == ClientServerKind.PROREQ) {
					for (Interface mInterface : port.getReqInterface()) {
						if (!exists(comp, mInterface, false)){
							Dependency dep = comp.createDependency(mInterface);
							StringBuilder s = new StringBuilder(25);
							s.append(comp.getName()).append("_Dpn_").append(mInterface.getName());
							dep.setName(s.toString());
							//createGraphicalDependency(comp, dep);
						}
					}
				}
			}

			private boolean exists(Component comp, Interface mInterface,
					boolean interfaceRealization) {
				// interfaceRealization is false when we want to check for pure Dependencies
				if (interfaceRealization) {
					for (InterfaceRealization ir : comp
							.getInterfaceRealizations()) {
						if (ir.getContract().equals(mInterface)) {
							return true;
						}
					}
				} else {
					for (Dependency dp : comp.getClientDependencies()){
						if (dp.getClass().equals(DependencyImpl.class)){
							for (NamedElement i : dp.getSuppliers()) {
								if (i instanceof Interface && mInterface.equals(i))
									return true;
							}
						}
					}
				}

				return false;
			}
		};
	}
	
	/**
	 * Removes dependencies from the model
	 *
	 * @param domain the TransactionalEditingDomain of the model
	 * @param deps the dependencies to be removed
	 * @return the command
	 */
	public static Command destroyDependencies(TransactionalEditingDomain domain, final EList<? extends Dependency> deps){
		return new RecordingCommand(domain) {
			@Override
			protected void doExecute() {
				for (Dependency dependency : deps) {
					dependency.destroy();
				}
			}
		};
	}
	
	
	/**
	 * Adds all the operations contained in a Classifier interf to a Component comp
	 *
	 * @param domain the TransactionalEditingDomain of the model
	 * @param interf the Classifier containing the operations
	 * @param comp the Component where the operation will be copied
	 * @return the command
	 */
	public static Command addOperationCommand(
			TransactionalEditingDomain domain, final Classifier interf, final Component comp) {
		
		return new RecordingCommand(domain) {

			@Override
			protected void doExecute() {
				for (Operation operation : interf.getAllOperations()) {
					if (!UMLUtils.isOperationContained(comp, operation)) {
						Operation newOp = comp.createOwnedOperation(operation.getName(),null,null);
						UMLUtils.copyOperation(operation, newOp);
						createGraphicalOperation(comp, newOp);
					}
				}
				
			}
			
		};
	}
	
	
	/**
	 * Adds all the ports contained in a Component providerComp to a Component clientComp
	 *
	 * @param domain the TransactionalEditingDomain of the model
	 * @param providerComp the Component containing the ports
	 * @param clientComp the Component where the ports will be copied
	 * @return the command
	 */
public static Command addAllPortsCommand(TransactionalEditingDomain domain, final Component providerComp, final Component clientComp) {
		return new RecordingCommand(domain) {
			@Override
			protected void doExecute() {
				//addClientServerPort(comp, interf.getClientDependencies());
				Profile gcm = ViewUtils.getView(clientComp).getAppliedProfile("MARTE::MARTE_DesignModel::GCM");
				if (gcm == null) 
					return;
				Stereotype cspStereotype = gcm.getOwnedStereotype("ClientServerPort");
				
				for (Port supplierPort : providerComp.getOwnedPorts()) {
					ClientServerPort cspSupplied = UMLUtils.getStereotypeApplication(supplierPort, ClientServerPort.class);
					if (cspSupplied == null)
						continue;
					if (clientComp.getOwnedPort(supplierPort.getName(), supplierPort.getType()) == null){
						Port clientPort = clientComp.createOwnedPort(supplierPort.getName(), supplierPort.getType());
						ClientServerPort cspClient = (ClientServerPort) clientPort.applyStereotype(cspStereotype);
						cspClient.setKind(cspSupplied.getKind());
						clientPort.setValue(cspStereotype, "provInterface", supplierPort.getValue(cspStereotype, "provInterface"));
						clientPort.setValue(cspStereotype, "reqInterface", supplierPort.getValue(cspStereotype, "reqInterface"));
					}
					
				}
				
				//flow ports!
				EList<Port> typePortList = providerComp.getOwnedPorts();
				for (Port typePort : typePortList) {
					Stereotype typeFlowPort = typePort.getAppliedStereotype("MARTE::MARTE_DesignModel::GCM::FlowPort");
//					EList<Dependency> typePortMappings = typePort.getClientDependencies();
					//if port doesn't already exists in the implementation (client)
					if (clientComp.getOwnedPort(typePort.getName(), typePort.getType()) == null){
						//create it!
						Port implPort = clientComp.createOwnedPort(typePort.getName(), typePort.getType());
						//apply <<FlowPort>> and set "direction"
						implPort.applyStereotype(typeFlowPort);
						Stereotype implFlowPort = implPort.getAppliedStereotype("MARTE::MARTE_DesignModel::GCM::FlowPort");
						implPort.setValue(implFlowPort, "direction", typePort.getValue(typeFlowPort, "direction"));
						
						//copy the mappings: SKIPPED FOR THE MOMENT -  mappings are defined at ComponentType level
//						Package owningPkg = implPort.allOwningPackages().get(0);
//						for (Dependency dep : typePortMappings) {
//							if(dep instanceof Abstraction && dep.getAppliedStereotype("MARTE::MARTE_Foundations::Alloc::Allocate") != null){
//								Abstraction abs = UMLFactory.eINSTANCE.createAbstraction();
//								
////								abs.getClients().add();
//								abs.getSuppliers().add(implPort);
//								abs.setName(dep.getName());
//								owningPkg.getPackagedElements().add(abs);
//								//apply <<Allocate>> stereotype
//								MARTEProfileManager.applyStereotypeTo("allocate", abs);
//							}
//						}
					}
				}
				

			}
			
		};
	}

	
	
	

/**
 * Adds the specified supplierPort contained in a Component providerComp to a Component clientComp
 *
 * @param domain the TransactionalEditingDomain of the model
 * @param providerComp the Component containing the port
 * @param clientComp the Component where the port will be copied
 * @param supplierPort the supplier port
 * @return the command
 */
public static Command addPortCommand(TransactionalEditingDomain domain, final Component providerComp, final Component clientComp, final Port supplierPort) {
	return new RecordingCommand(domain) {
			@Override
			protected void doExecute() {
				//addClientServerPort(comp, interf.getClientDependencies());
				Profile gcm = ViewUtils.getView(clientComp).getAppliedProfile("MARTE::MARTE_DesignModel::GCM");
				if (gcm == null) 
					return;
				Stereotype cspStereotype = gcm.getOwnedStereotype("ClientServerPort");
				ClientServerPort cspSupplied = UMLUtils.getStereotypeApplication(supplierPort, ClientServerPort.class);
				if (cspSupplied == null)
					return;
				if (clientComp.getOwnedPort(supplierPort.getName(), supplierPort.getType()) == null){
					Port clientPort = clientComp.createOwnedPort(supplierPort.getName(), supplierPort.getType());
					ClientServerPort cspClient = (ClientServerPort) clientPort.applyStereotype(cspStereotype);
					cspClient.setKind(cspSupplied.getKind());
					clientPort.setValue(cspStereotype, "provInterface", supplierPort.getValue(cspStereotype, "provInterface"));
					clientPort.setValue(cspStereotype, "reqInterface", supplierPort.getValue(cspStereotype, "reqInterface"));
				}
			}
		};
	}
	
	
	//add operation to the interface's clients
	/**
	 * Copies an operation from an Interface to all the Component Type that realizes the Interface
	 * and to all the ComponentImplementation that realizes the ComponentType(s)
	 *
	 * @param domain the TransactionalEditingDomain of the model
	 * @param interf the Interface containing the operation
	 * @param operation the Operation to be copied
	 * @return the command
	 */
	public static Command addOperationCommand1(TransactionalEditingDomain domain, final Interface interf, final Operation operation){
		return new RecordingCommand(domain) {

			@Override
			protected void doExecute() {
				EList<NamedElement> relationships = UMLUtils.getInterfaceClients(interf);
				if (relationships != null && !relationships.isEmpty()) {
					for (NamedElement element : relationships) {
						if (element instanceof Component){
							Component comp = (Component)element;
							for (NamedElement compImpl : UMLUtils.getComponentImplementations(comp)) {
								if (compImpl instanceof Component){
									Operation newOp = ((Class) compImpl).createOwnedOperation(operation.getName(),null,null);
									UMLUtils.copyOperation(operation, newOp);
									createGraphicalOperation((Component) compImpl, newOp);
								}
							}
							//TODO try to avoid a possible Papyrus Bug (to be confirmed)
							System.out.println("DEBUG size: " + comp.getOperations().size());
								Operation newOp = comp.createOwnedOperation(operation.getName(),null,null);
								UMLUtils.copyOperation(operation, newOp);

								createGraphicalOperation(comp, newOp);
						}
					}
				}
				
			}
			
		};
	}
	
	
	/**
	 * Creates a graphical action for a new Operation added to to a Component
	 *
	 * @param containerComp the container Component
	 * @param newOp the new Operation
	 */
	private static void createGraphicalOperation(Component containerComp, Operation newOp){
		IMultiDiagramEditor editor = EditorUtils.getMultiDiagramEditor();
		/*ISashWindowsContainer container = null;
		try {
			container = ServiceUtils.getInstance().getISashWindowsContainer(editor.getServicesRegistry());
		} catch (ServiceException e) {
			return;
		}
		Object model = container.getActiveSashWindowsPage().getRawModel();
		Diagram diagram = null;
		if (model instanceof PageRef) {
			EObject diagramEObj = ((PageRef) model).getEmfPageIdentifier();
			if (diagramEObj instanceof Diagram) {
				diagram = (Diagram) diagramEObj;
			}
		}*/
		Diagram diagram = (Diagram)editor.getAdapter(Diagram.class);
		DiagramEditPart diagramEP = (DiagramEditPart)editor.getAdapter(DiagramEditPart.class);
		IDiagramGraphicalViewer viewer = (IDiagramGraphicalViewer)diagramEP.getViewer();
		
		DropObjectsRequest dropObjectsRequest = new DropObjectsRequest();
		dropObjectsRequest.setLocation(new Point(1, 1));
		ArrayList<Object> objectList = new ArrayList<Object>();
		objectList.add(newOp);
		dropObjectsRequest.setObjects(objectList);
		@SuppressWarnings("unchecked")
		List<EditPart> editParts = (List<EditPart>)viewer.findEditPartsForElement(
				EMFCoreUtil.getProxyID(containerComp),
				INodeEditPart.class
			);
		
		StringBuilder s = new StringBuilder();
		
		s.delete(0, s.length());
		s.append("CREATING GUI OP");
		CHESSProjectSupport.CHESS_CONSOLE.println(s.toString());
		
		
		s.delete(0, s.length());
		s.append("EDITPARTS " + editParts);
		CHESSProjectSupport.CHESS_CONSOLE.println(s.toString());
		
		s.delete(0, s.length());
		s.append("EDITPARTS SIZE " + editParts.size());
		CHESSProjectSupport.CHESS_CONSOLE.println(s.toString());
		
		if(editParts.size() >0){
		List<Object> childrenEparts = editParts.get(0).getChildren();
			ComponentOperationCompartmentEditPart compAttrEpart = null;
			for(int i=0; i<childrenEparts.size(); i++){
				if(childrenEparts.get(i) instanceof ComponentOperationCompartmentEditPart){
					compAttrEpart = (ComponentOperationCompartmentEditPart)childrenEparts.get(i);
					org.eclipse.gef.commands.Command cmd = compAttrEpart.getTargetEditPart(dropObjectsRequest).getCommand(dropObjectsRequest);
					//org.eclipse.gef.commands.Command cmd = compAttrEpart.getCommand(dropObjectsRequest);
					s.delete(0, s.length());
					s.append("EXEC GUI COMMAND" + editParts);
					CHESSProjectSupport.CHESS_CONSOLE.println(s.toString());
					diagramEP.getDiagramEditDomain().getDiagramCommandStack().execute(cmd);
				}  
			}
		}
		
	}
	
	/**
	 * Creates a graphical action for a new Port added to to a Component
	 *
	 * @param containerComp the container Component
	 * @param port the new Port
	 */
	private static void createGraphicalPort(Component containerComp, Port port){
		IMultiDiagramEditor editor = EditorUtils.getMultiDiagramEditor();
		Diagram diagram = (Diagram)editor.getAdapter(Diagram.class);
		DiagramEditPart diagramEP = (DiagramEditPart)editor.getAdapter(DiagramEditPart.class);
		IDiagramGraphicalViewer viewer = (IDiagramGraphicalViewer)diagramEP.getViewer();
		DropObjectsRequest dropObjectsRequest = new DropObjectsRequest();
		dropObjectsRequest.setLocation(new Point(1, 1));
		ArrayList<Object> objectList = new ArrayList<Object>();
		objectList.add(port);
		dropObjectsRequest.setObjects(objectList);
		@SuppressWarnings("unchecked")
		List<EditPart> editParts = (List<EditPart>)viewer.findEditPartsForElement(
				EMFCoreUtil.getProxyID(containerComp),
				INodeEditPart.class
			);
		
		if ( editParts.size() > 0 )
		{
			List<Object> childrenEparts = editParts.get(0).getChildren();
			ComponentCompositeCompartmentEditPart compAttrEpart = null;
			for(int i=0; i<childrenEparts.size(); i++)
			{		
				if(childrenEparts.get(i) instanceof ComponentCompositeCompartmentEditPart){
					compAttrEpart = (ComponentCompositeCompartmentEditPart)childrenEparts.get(i);
					
					org.eclipse.gef.commands.Command cmd = compAttrEpart.getCommand(dropObjectsRequest);
					
					diagramEP.getDiagramEditDomain().getDiagramCommandStack().execute(cmd);
					
				}  
			}
		}
	}
	
	
	/**
	 * Adds the specified Comment stereotyped with Assign to the assignList of the deployment view
	 *
	 * @param domain the TransactionalEditingDomain of the model
	 * @param comm the Comment stereotyped as <<Assign>> to be added
	 * @return the command
	 */
	public static Command addCommentCommand(
			TransactionalEditingDomain domain, final Comment comm) {
		
		return new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				
				PapyrusMultiDiagramEditor editor = CHESSEditorUtils.getCHESSEditor();
				if (editor == null)
					return;
				
				if(comm.getAppliedStereotype("MARTE::MARTE_Foundations::Alloc::Assign")!= null){
					try {
						Resource res = ResourceUtils.getUMLResource(editor.getServicesRegistry());
						Model umlModel = ResourceUtils.getModel(res);
						
						Package deplViewPkg = (Package) umlModel.getPackagedElement("modelDeploymentView");
						Stereotype deplView = deplViewPkg.getAppliedStereotype("CHESS::Core::CHESSViews::DeploymentView");
						if (deplView != null){
							EList<Comment> tmp = (EList<Comment>) deplViewPkg.getValue(deplView, "AssignList");
							tmp.add(comm);
							//editor.getDiagramStatus().setUserAction(false);
							//System.out.println(deplViewPkg.getValue(deplView, "AssignList").toString());
						}
						
					} catch (ServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};
	}
		
	/**
	 * Removes the specified Comment stereotyped with Assign to the assignList of the deployment view
	 *
	 * @param domain the TransactionalEditingDomain of the model
	 * @param comm the Comment stereotyped as <<Assign>> to be removed
	 * @return the command
	 */
	public static Command removeCommentCommand(
			TransactionalEditingDomain domain, final Comment comm) {

		return new RecordingCommand(domain) {

			@Override
			protected void doExecute() {
				PapyrusMultiDiagramEditor editor = CHESSEditorUtils.getCHESSEditor();
				if (editor == null)
					return;
				
				if(comm.getAppliedStereotype("MARTE::MARTE_Foundations::Alloc::Assign")!= null){

					try {
						Resource res = ResourceUtils.getUMLResource(editor.getServicesRegistry());
						Model umlModel = ResourceUtils.getModel(res);

						Package deplViewPkg = (Package) umlModel.getPackagedElement("modelDeploymentView");
						Stereotype deplView = deplViewPkg.getAppliedStereotype("CHESS::Core::CHESSViews::DeploymentView");
						if (deplView != null){
							EList<Comment> tmp = (EList<Comment>) deplViewPkg.getValue(deplView, "AssignList");
							tmp.remove(comm);
						}

					} catch (ServiceException e) {
						e.printStackTrace();
					}

				}

			}
		};

	}
	
	/**
	 * Copies the specified parameter added to an operation of an interface to the 
	 * corresponding operation in the Components that realize the interface
	 *
	 * @param opCont the Interface containing the operation
	 * @param param the Parameter to be copied
	 * @param operation the Operation containing the parameter
	 * @param TransactionalEditingDomain of the model
	 * @return the command
	 */
	public static Command addParameterCommand(final Interface opCont, final Parameter param, final Operation operation, TransactionalEditingDomain domain) {
		return new RecordingCommand(domain) {
		
			@Override
			protected void doExecute() {
				
				EList<Component> relationships = UMLUtils.getAllInterfaceComponents(opCont);
				if (relationships == null || relationships.isEmpty()) 
					return;
				for (Component comp : relationships) {
					for (Operation op : comp.getOwnedOperations()) {
						if (UMLUtils.areOperationsEqual(operation, op, param)) {
							Parameter tmp = op.createOwnedParameter(param.getName(),param.getType());
							tmp.setDirection(param.getDirection());
							tmp.setEffect(param.getEffect());
						}
					}
				}
			}
		};
	}
	
	
	/**
	 * Updates the ports, stereotyped as <<ClientServerPort>>, of the ComponentImplementations
	 * 
	 * @see addAllPortsCommand
	 *
	 * @param domain the TransactionalEditingDomain of the model
	 * @param cspSupplied the Port stereotyped as <<ClientServerPort>>
	 * @return the command
	 */
	public static Command updateImplementationsPorts(
			TransactionalEditingDomain domain, final ClientServerPort cspSupplied) {
		
		return new RecordingCommand(domain) {
			
			@Override
			protected void doExecute() {
				Port supplierPort = cspSupplied.getBase_Port();
				Element supplierComp = supplierPort.getOwner();
				if (!UMLUtils.isComponentType(supplierComp))
					return;
				
				Profile gcm = ViewUtils.getView(supplierComp).getAppliedProfile("MARTE::MARTE_DesignModel::GCM");
				if (gcm == null) 
					return;
				Stereotype cspStereotype = gcm.getOwnedStereotype("ClientServerPort");
				
				for(Component clientComp: UMLUtils.getComponentImplementations((Component) supplierComp)){
					Port clientPort = clientComp.getOwnedPort(supplierPort.getName(), supplierPort.getType());
					if (clientPort == null){
						clientPort = clientComp.createOwnedPort(supplierPort.getName(), supplierPort.getType());
						MARTEProfileManager.applyStereotypeTo("ClientServerPort", clientPort);
					}
//						return;
					ClientServerPort cspClient = (ClientServerPort) clientPort.getStereotypeApplication(cspStereotype);
					cspClient.setKind(cspSupplied.getKind());
					clientPort.setValue(cspStereotype, "provInterface", supplierPort.getValue(cspStereotype, "provInterface"));
					clientPort.setValue(cspStereotype, "reqInterface", supplierPort.getValue(cspStereotype, "reqInterface"));
				}
			}
		};
	}
	
	
	/**
	 * Updates the ports, stereotyped as <<FlowPort>>, of the ComponentImplementations
	 * 
	 * @see addAllPortsCommand
	 *
	 * @param domain the TransactionalEditingDomain of the model
	 * @param flPort the Port stereotyped as <<FlowPort>>
	 * @return the command
	 */
	public static Command updateImplementationsFlowPorts(
			TransactionalEditingDomain domain, final FlowPort flPort) {
		
		return new RecordingCommand(domain) {

			@Override
			protected void doExecute() {
				// TODO Auto-generated method stub
				Port portType = flPort.getBase_Port();
				Element compType = portType.getOwner();
				if (!UMLUtils.isComponentType(compType))
					return;
				
				Stereotype flpType = portType.getAppliedStereotype("MARTE::MARTE_DesignModel::GCM::FlowPort");
				
				for(Component compImpl : UMLUtils.getComponentImplementations((Component) compType)){
					Port portImpl = compImpl.getOwnedPort(portType.getName(), portType.getType());
					if (portImpl == null){
						portImpl = compImpl.createOwnedPort(portType.getName(), portType.getType());
						MARTEProfileManager.applyStereotypeTo("FlowPort", portImpl);
					}
					Stereotype flpImpl = portImpl.getAppliedStereotype("MARTE::MARTE_DesignModel::GCM::FlowPort");
					portImpl.setValue(flpImpl, "direction", portType.getValue(flpType, "direction"));
				}
			}
			
		};

	}
	
	
	/**
	 * Removes the specified port of the ComponentImplementations
	 *
	 * @param domain the TransactionalEditingDomain of the model
	 * @param supplierPort the Port to be removed
	 * @return the command
	 */
	public static Command removeImplementationsPorts(
			TransactionalEditingDomain domain, final Port supplierPort) {
		
		return new RecordingCommand(domain) {
			
			@Override
			protected void doExecute() {
				Element supplierComp = supplierPort.getOwner();
				if (!UMLUtils.isComponentType(supplierComp))
					return;
				
				for(Component clientComp: UMLUtils.getComponentImplementations((Component) supplierComp)){
					Port clientPort = clientComp.getOwnedPort(supplierPort.getName(), supplierPort.getType());
					if (clientPort == null)
						return;
					clientPort.destroy();
				}
			}
		};
	}

	
}
