/*******************************************************************************
 * Copyright (C) 2017 Fondazione Bruno Kessler.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Alberto Debiasi - initial API and implementation
 ******************************************************************************/
package org.polarsys.chess.service.gui.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import eu.fbk.eclipse.standardtools.utils.ui.utils.DirectoryUtil;

/**
 * 
 *
 */
public class DialogUtils {

	private DirectoryUtil directoryUtils = DirectoryUtil.getInstance();
	private static DialogUtils exportDialogUtils;

	public static DialogUtils getInstance() {
		if (exportDialogUtils == null) {
			exportDialogUtils = new DialogUtils();
		}
		return exportDialogUtils;
	}

	
	
	public String getDirectoryNameFromDialog() throws Exception {	
		return getDirectoryNameFromDialog("Select the directory");
	}

	public String getDirectoryNameFromDialog(String title) throws Exception {
		return getDirectoryNameFromDialog(title, directoryUtils.getCurrentProjectDir());
	}
	
	public String getDirectoryNameFromDialog(String title, String path) throws Exception {
		final Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

		final DirectoryDialog directoryDialog = new DirectoryDialog(shell);
		directoryDialog.setText(title);
		directoryDialog.setFilterPath(path);
		final String directoryName = directoryDialog.open();

		return directoryName;
	}
	
	public String getFileNameFromDialog() throws Exception {	
		return getFileNameFromDialog("Select the file", null);
	}

	public String getFileNameFromDialog(String title, String filterExtension) throws Exception {
		final Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

		final FileDialog fileDialog = new FileDialog(shell);
		fileDialog.setText(title);
		fileDialog.setFilterPath(directoryUtils.getCurrentProjectDir());
		if(filterExtension != null) {
			final String[] extensions = {filterExtension};
			fileDialog.setFilterExtensions(extensions);
		}
		final String directoryName = fileDialog.open();

		return directoryName;
	}
	
	public List<String> getFileNamesFromDialog(String title, String filterExtension) throws Exception {
		final Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

		final FileDialog fileDialog = new FileDialog(shell, SWT.MULTI);
		List<String> files = new ArrayList<String>();
		fileDialog.setText(title);
		fileDialog.setFilterPath(directoryUtils.getCurrentProjectDir());
		if(filterExtension != null) {
			final String[] extensions = {filterExtension};
			fileDialog.setFilterExtensions(extensions);
		}

	    if (fileDialog.open() != null) {
	    	final String[] names = fileDialog.getFileNames();
	        for (int i = 0, n = names.length; i < n; i++) {
	          final StringBuffer buf = new StringBuffer(fileDialog.getFilterPath());
	          if (buf.charAt(buf.length() - 1) != File.separatorChar) {
	            buf.append(File.separatorChar);
	          }
	          buf.append(names[i]);
	          files.add(buf.toString());
	        }
	      }
		
		return files;
	}
}
