/*******************************************************************************
 * Copyright (C) 2020 Fondazione Bruno Kessler.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 ******************************************************************************/
package org.polarsys.chess.contracts.verificationService.test.runtime.tests;

import java.io.File;
import java.nio.file.Paths;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.junit.framework.classification.tests.AbstractPapyrusTest;
import org.eclipse.papyrus.junit.framework.runner.Headless;
import org.eclipse.papyrus.junit.utils.rules.ModelSetFixture;
import org.eclipse.papyrus.junit.utils.rules.PluginResource;
import org.eclipse.papyrus.junit.utils.rules.ResourceSetFixture;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.polarsys.chess.OSSImporter.core.actions.ImportOSSFileAction;
import org.polarsys.chess.contracts.profile.chesscontract.util.EntityUtil;
import org.polarsys.chess.contracts.verificationService.test.runtime.util.TestResultsUtil;
import org.polarsys.chess.service.core.model.ChessSystemModel;
import org.polarsys.chess.service.core.model.UMLStateMachineModel;

import eu.fbk.eclipse.standardtools.ModelTranslatorToOcra.core.services.OSSTranslatorServiceAPI;
import eu.fbk.eclipse.standardtools.StateMachineTranslatorToSmv.core.services.K2TranslatorServiceAPI;
import eu.fbk.eclipse.standardtools.StateMachineTranslatorToSmv.core.services.SMVTranslatorServiceAPI;
import eu.fbk.tools.adapter.ui.preferences.PreferenceConstants;

@Headless
public class TestBasicOperationsHeadless extends AbstractPapyrusTest {

	@Rule
	public ErrorCollector collector = new ErrorCollector();

	private String testOutput;
	private EntityUtil entityUtil = EntityUtil.getInstance();
	private static final Logger logger = Logger.getLogger(TestBasicOperationsHeadless.class);

	private final String emptyProjectFolderPath = "resources/EmptyProject/";
	private final String emptyProjectPath = "resources/EmptyProject/EmptyProject.di";
		
	private final String projectFolderPath = "resources/SSR_fi/";
	private final String projectPath = projectFolderPath + "SSR.di";

	private final String timedProjectFolderPath = "resources/TimedModel/";
	private final String timedProjectPath = timedProjectFolderPath + "train-gate-controller.di";
	
	private final String asyncProjectFolderPath = "resources/AsyncProject/";
	private final String asyncProjectPath = asyncProjectFolderPath + "AsyncProject.di";
	
	@Rule
	public final ModelSetFixture modelSet = new ModelSetFixture();
	
	@Rule
	public final ResourceSetFixture resourceSetFixture = new ResourceSetFixture();

	@Test
	@PluginResource(projectPath)
	//@Ignore
	public void testExportModelAsOss() throws Exception {
		Class umlSelectedComponent = getSystemComponent();

		File ossFile = exportModelAsOss(umlSelectedComponent, testOutput, false, 1);

		String oracleFilePath = projectFolderPath + "NuSMV3-OCRA/Files/System.oss";
		TestResultsUtil.filesAreEqual(Paths.get(ossFile.getPath()), Paths.get(oracleFilePath), collector);

	}
	
	@Test
	@PluginResource(asyncProjectPath)
	//@Ignore
	public void testExportAsyncModelAsOss() throws Exception {
		Class umlSelectedComponent = getSystemComponent();

		File ossFile = exportModelAsOss(umlSelectedComponent, testOutput, true, 1);

		String oracleFilePath = asyncProjectFolderPath + "OCRAFiles/SystemWithWatchdog_Discrete.oss";
		TestResultsUtil.filesAreEqual(Paths.get(ossFile.getPath()), Paths.get(oracleFilePath), collector);

	}
	
	@Test
	@PluginResource(asyncProjectPath)
	//@Ignore
	public void testExportAsyncHybridModelAsOss() throws Exception {
		Class umlSelectedComponent = getSystemComponent();

		File ossFile = exportModelAsOss(umlSelectedComponent, testOutput, true, 0);

		String oracleFilePath = asyncProjectFolderPath + "OCRAFiles/SystemWithWatchdog_Hybrid.oss";
		TestResultsUtil.filesAreEqual(Paths.get(ossFile.getPath()), Paths.get(oracleFilePath),collector);

	}
	
	@Test
	@PluginResource(emptyProjectPath)
	//@Ignore
	public void testImportModelFromOssFile() throws Exception{
		
		Model model = getModel();
		
		//openEditor(model);
		
		Package umlPackage = entityUtil.getSystemViewPackage(model);
		//EntityUtil.getInstance().getCurrentSystemView();
		File ossInputFile = new File("resources//EmptyProject//OssFile//System.oss");
		
		final ImportOSSFileAction action = ImportOSSFileAction.getInstance();

		if (action != null) {
	
			TransactionalEditingDomain domain = modelSet.getEditingDomain();
			System.out.println("domain: "+domain);
			// Parse the file and retrieve results
			action.startParsing((Package) umlPackage, ossInputFile,domain);
			
			Class umlSelectedComponent = entityUtil.getSystemComponent(umlPackage);
			TestBasicOperationsHeadless.exportModelAsOss(umlSelectedComponent, testOutput, false, 1);

			File outputFolder = new File(testOutput);
			String selectedDirectory = outputFolder.getAbsolutePath();
			
			String oracleFolder = emptyProjectFolderPath + "/OssFile";
			
			TestResultsUtil.dirsAreEqual(oracleFolder, selectedDirectory, collector);

			
			}
	}

	public static File exportModelAsOss(Class umlSelectedComponent,String testOutput, boolean isAsyncCommunication, int timeSpecification) throws Exception {
		
		OSSTranslatorServiceAPI ossTranslatorServiceAPI = OSSTranslatorServiceAPI
				.getInstance(ChessSystemModel.getInstance());
		System.out.println("umlSelectedComponent: " + umlSelectedComponent);		
		logger.debug("exportRootComponentAsOssModel");
		Object ocraModel = ossTranslatorServiceAPI.exportRootComponentToOssModel(umlSelectedComponent,
				timeSpecification, isAsyncCommunication, new NullProgressMonitor());
		logger.debug("generateOssFileFromOssModel");
		String fileName = ossTranslatorServiceAPI.getFileName(umlSelectedComponent);
		File ossFile = ossTranslatorServiceAPI.exportOSSModelToOSSFile(ocraModel, fileName, testOutput);
		return ossFile;
	}

	private Class getSystemComponent() throws Exception {
		Model model = getModel();
		Package umlSelectedPackage = entityUtil.getSystemViewPackage(model);

		System.out.println("umlSelectedPackage: " + umlSelectedPackage);

		Class umlSelectedComponent = entityUtil.getSystemElement(model);
		return umlSelectedComponent;
	}

	@Test
	@PluginResource(projectPath)
	//@Ignore
	public void testExportStateMachinesAsSmv() throws Exception {
		Model model = getModel();
		
		SMVTranslatorServiceAPI smvTranslatorService = SMVTranslatorServiceAPI
				.getInstance(ChessSystemModel.getInstance(), UMLStateMachineModel.getInstance());

		String selectedDirectory = testOutput;//"testOutput";

		Set<?> stateMachines = entityUtil.getNominalStateMachines(model);
		logger.debug("stateMachines.size: " + stateMachines.size());
		for (Object stateMachine : stateMachines) {
			smvTranslatorService.exportStateMachineToSmvFile(stateMachine, selectedDirectory, null,
					new NullProgressMonitor(),1);
		}

		String oracleFolder = projectFolderPath + "/SmvFiles";
		TestResultsUtil.dirsAreEqual(oracleFolder, selectedDirectory, collector);
	}
	
	@Test
	@PluginResource(timedProjectPath)
	//@Ignore
	public void testExportTimedStateMachinesAsSmv() throws Exception {
		Model model = getModel();
		
		SMVTranslatorServiceAPI smvTranslatorService = SMVTranslatorServiceAPI
				.getInstance(ChessSystemModel.getInstance(), UMLStateMachineModel.getInstance());

		String selectedDirectory = testOutput;//"testOutput";

		Set<?> stateMachines = entityUtil.getNominalStateMachines(model);
		logger.debug("stateMachines.size: " + stateMachines.size());
		for (Object stateMachine : stateMachines) {
			smvTranslatorService.exportStateMachineToSmvFile(stateMachine, selectedDirectory, null,
					new NullProgressMonitor(),2);
		}

		String oracleFolder = timedProjectFolderPath + "/SmvFiles";
		TestResultsUtil.dirsAreEqual(oracleFolder, selectedDirectory, collector);
	}
	
	@Test
	@PluginResource(projectPath)
	//@Ignore
	public void testExportBlocksAsK2() throws Exception {
		Model model = getModel();
		
		K2TranslatorServiceAPI k2TranslatorService = K2TranslatorServiceAPI
				.getInstance(ChessSystemModel.getInstance(), UMLStateMachineModel.getInstance());

		String selectedDirectory = testOutput;//"testOutput";
		
		Set<?> stateMachines = entityUtil.getNominalStateMachines(model);
		logger.debug("stateMachines.size: " + stateMachines.size());
		for (Object stateMachine : stateMachines) {
			Class blockAsClass = (Class) entityUtil.getOwner((Element) stateMachine);
			k2TranslatorService.exportBlockToK2File(blockAsClass, selectedDirectory, blockAsClass.getName(), null, new NullProgressMonitor());
		}

		String oracleFolder = projectFolderPath + "/KratosFiles";
		TestResultsUtil.dirsAreEqual(oracleFolder, selectedDirectory, collector);
	}

	@Before
	public void setTestParameters() throws Exception {
		testOutput = TestResultsUtil.cleanDirectory("testOutputBasicOperationsHeadless");
		eu.fbk.tools.adapter.ui.Activator.getDefault().getPreferenceStore().setValue(
				PreferenceConstants.OSLC_ENABLED, false);
	}

	Model getModel() {
		return (Model) resourceSetFixture.getModel();
	}
	
}
