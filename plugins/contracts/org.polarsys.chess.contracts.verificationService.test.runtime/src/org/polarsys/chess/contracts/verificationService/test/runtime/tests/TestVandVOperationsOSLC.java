/*******************************************************************************
 * Copyright (C) 2020 Fondazione Bruno Kessler.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 ******************************************************************************/
package org.polarsys.chess.contracts.verificationService.test.runtime.tests;

import org.eclipse.lyo.client.oslc.OSLCConstants;
import org.junit.Before;
import org.junit.Ignore;
import org.polarsys.chess.contracts.verificationService.test.runtime.util.TestResultsUtil;

import eu.fbk.tools.adapter.ui.preferences.PreferenceConstants;


public class TestVandVOperationsOSLC extends TestVAndVOperations{
	
	@Before
	@Override
	public void setTestParameters() throws Exception {
		super.setTestParameters();
		testOutput = TestResultsUtil.cleanDirectory("testOutputVandVOperations");
		eu.fbk.tools.adapter.ui.Activator.getDefault().getPreferenceStore().setValue(
				PreferenceConstants.OSLC_ENABLED, true);
		eu.fbk.tools.adapter.ui.Activator.getDefault().getPreferenceStore().setValue(
				PreferenceConstants.OSLC_SP_CATALOG_BASEURL, "http://waters.fbk.eu:8080");
		eu.fbk.tools.adapter.ui.Activator.getDefault().getPreferenceStore().setValue(
				PreferenceConstants.OSLC_SP_CATALOG_PATH, "/oslc4j-registry");
		eu.fbk.tools.adapter.ui.Activator.getDefault().getPreferenceStore().setValue(
				PreferenceConstants.OSLC_SP_CATALOG_HOST,"waters");
		eu.fbk.tools.adapter.ui.Activator.getDefault().getPreferenceStore().setValue(
				PreferenceConstants.OSLC_SP_CATALOG_PORT,"8080");
		eu.fbk.tools.adapter.ui.Activator.getDefault().getPreferenceStore().setValue(
				OSLCConstants.OSLC_AUTO,"2");		
	}

	@Override
	@Ignore
	public void testCheckContractRefinementOnInstantiateArch() throws Exception {
		//super.testCheckContractRefinementOnInstantiateArch();
	}
	
	
}
