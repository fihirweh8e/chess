/*******************************************************************************
 * Copyright (C) 2017 Fondazione Bruno Kessler.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Alberto Debiasi - initial API and implementation
 ******************************************************************************/
/**
 * 
 */
package org.polarsys.chess.verificationService.ui.commands;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.uml2.uml.Class;

import eu.fbk.eclipse.standardTools.KratosExecService.ui.utils.KratosDirectoryUtil;
import eu.fbk.eclipse.standardtools.StateMachineTranslatorToSmv.ui.services.K2ExportServiceUI;
import eu.fbk.eclipse.standardtools.utils.ui.commands.AbstractJobCommand;
import org.polarsys.chess.service.core.model.ChessSystemModel;
import org.polarsys.chess.service.core.model.UMLStateMachineModel;
import org.polarsys.chess.service.gui.utils.SelectionUtil;

/**
 * 
 *
 */
public class ExportBlockToK2FileCommand extends AbstractJobCommand {

	private SelectionUtil selectionUtil = SelectionUtil.getInstance();
	private K2ExportServiceUI k2ExportService = K2ExportServiceUI
			.getInstance(ChessSystemModel.getInstance(),UMLStateMachineModel.getInstance());
	private KratosDirectoryUtil ocraDirectoryUtil = KratosDirectoryUtil.getInstance();

	public ExportBlockToK2FileCommand() {
		super("Export Model To .K2 File");
	}

	private String k2Filepath;
	private boolean showPopups;
	private Class umlSelectedComponent;

	@Override
	public void execPreJobOperations(ExecutionEvent event, IProgressMonitor monitor) throws Exception {
		umlSelectedComponent = selectionUtil.getUmlComponentFromSelectedObject(event);
		k2Filepath = ocraDirectoryUtil.getK2DirPath();
		showPopups = true;
	}

	@Override
	public void execJobCommand(ExecutionEvent event, IProgressMonitor monitor) throws Exception {

		k2ExportService.exportSingleBlock(umlSelectedComponent, showPopups, null, k2Filepath, monitor);

	}

	@Override
	public void execPostJobOperations(ExecutionEvent event, NullProgressMonitor nullProgressMonitor) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
