/*******************************************************************************
 * Copyright (C) 2021 Fondazione Bruno Kessler.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Alberto Debiasi - initial API and implementation
 ******************************************************************************/
package org.polarsys.chess.customEditors.editors.ocra;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EObjectTreeElement;
import org.eclipse.papyrus.sysml.blocks.Block;
import org.eclipse.papyrus.uml.textedit.property.xtext.ui.contributions.PropertyXtextDirectEditorConfiguration;
import org.eclipse.papyrus.uml.xtext.integration.DefaultXtextDirectEditorConfiguration;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.polarsys.chess.contracts.profile.chesscontract.util.EntityUtil;
import org.polarsys.chess.service.core.model.ChessSystemModel;

import com.google.inject.Injector;

import eu.fbk.eclipse.standardtools.xtextService.core.utils.XtextElementsUtil;
import eu.fbk.eclipse.standardtools.xtextService.ui.utils.XTextEditorViewer;
import eu.fbk.eclipse.standardtools.xtextService.ui.utils.XTextEditorViewer_constraint;
import eu.fbk.tools.editor.contract.expression.ui.internal.ExpressionActivator;

public class OcraPropertyEditor extends org.polarsys.chess.customEditors.editors.XtextLanguageEditor {

	@Override
	protected DefaultXtextDirectEditorConfiguration getConfigurationFromSelection() {
		EObject semanticElement = getEObject();
		if (semanticElement != null) {
			if (semanticElement instanceof OpaqueExpression) {
				DefaultXtextDirectEditorConfiguration xtextConfiguration = new OcraExpressionEditorConfiguration();
				xtextConfiguration.setLanguage("OCRA");
				xtextConfiguration.preEditAction(semanticElement);
				// AbstractSystemModel
				XtextElementsUtil xTextUtil = new XtextElementsUtil(ChessSystemModel.getInstance());

				Element currBlock = getNearestOwnerBlock((Element) semanticElement);

				XTextEditorViewer.cleanPorts();
				XTextEditorViewer.addPorts(xTextUtil.getXTextPortsFromComponent(currBlock));
				XTextEditorViewer.cleanParameters();
				XTextEditorViewer.addParameters(xTextUtil.getXTextParametersFromComponent(currBlock));
				XTextEditorViewer.cleanEnumValues();
				XTextEditorViewer.addEnumValues(
						ChessSystemModel.getInstance().getEnumValuesFromAttributes(currBlock).toArray(new String[0]));
				XTextEditorViewer.cleanDefines();
				XTextEditorViewer
						.addDefines(xTextUtil.getXTextDefinesNamesFromComponent(currBlock).toArray(new String[0]));

				if (EntityUtil.getInstance().isDelegationConstraint(((OpaqueExpression) semanticElement).getOwner())) {
					// find subcomponents name
					String[] subCompNames = ChessSystemModel.getInstance().getSubComponentsName(currBlock);
					// for each subcomponent name
					for (String subCompName : subCompNames) {
						// find in/out/inout ports name
						Object subComp = ChessSystemModel.getInstance().getSubComponent(currBlock, subCompName);

						EList<?> subCompPorts = ChessSystemModel.getInstance().getNonStaticPorts(subComp);
						if ((subCompPorts != null) && (subCompPorts.size() != 0)) {
							XTextEditorViewer_constraint.addPorts(subCompName, xTextUtil.getXTextPorts(subCompPorts));
						}

						EList<?> subCompStaticPorts = ChessSystemModel.getInstance().getStaticPorts(subComp);
						if ((subCompStaticPorts != null) && (subCompStaticPorts.size() != 0)) {
							XTextEditorViewer_constraint.addParameters(subCompName,
									xTextUtil.getXTextParametersFromStaticPorts(subCompStaticPorts));
						}

						EList<?> subCompUnintfuncts = ChessSystemModel.getInstance().getUninterpretedFunctions(subComp);
						if ((subCompUnintfuncts != null) && (subCompUnintfuncts.size() != 0)) {
							XTextEditorViewer_constraint.addParameters(subCompName,
									xTextUtil.getXTextParametersFromUninterpretedFunctions(subCompUnintfuncts));
						}
					}
				}

				return xtextConfiguration;
			}
		}
		return null;
	}

	private Element getNearestOwnerBlock(Element currElement) {
		while (true) {

			if (EntityUtil.getInstance().isBlock(currElement)) {
				break;
			}
			if (currElement instanceof Event) {
				Block currBlock = EntityUtil.getInstance().getTriggeredBlock((Event) currElement);
				if (currBlock != null) {
					currElement = currBlock.getBase_Class();
				}
			} else if (currElement.getOwner() != null) {
				currElement = currElement.getOwner();
			} else {
				currElement = getSelectionFromModelExplorer(currElement);
				if (currElement == null) {
					break;
				}
			}
		}
		return currElement;
	}

	private Element getSelectionFromModelExplorer(Element currElement) {
		IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (activeWorkbenchWindow != null) {
			// Get current selection
			ISelection selection = activeWorkbenchWindow.getSelectionService()
					.getSelection("org.eclipse.papyrus.views.modelexplorer.modelexplorer");

			// Get first element if the selection is an IStructuredSelection
			if (selection instanceof IStructuredSelection) {
				IStructuredSelection structuredSelection = (IStructuredSelection) selection;
				if (structuredSelection.getFirstElement() instanceof EObjectTreeElement) {
					currElement = (Element) ((EObjectTreeElement) structuredSelection.getFirstElement()).getEObject();
				}
			}
		}
		return currElement;
	}

}

class OcraExpressionEditorConfiguration extends PropertyXtextDirectEditorConfiguration {

	@Override
	public Injector getInjector() {
		System.out.println("PropertyXtextDirectEditorConfiguration.getInjector");
		return ExpressionActivator.getInstance()
				.getInjector(ExpressionActivator.EU_FBK_TOOLS_EDITOR_CONTRACT_EXPRESSION_EXPRESSION);
	}

}
