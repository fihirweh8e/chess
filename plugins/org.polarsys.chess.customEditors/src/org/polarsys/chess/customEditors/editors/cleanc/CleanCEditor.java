/*******************************************************************************
 * Copyright (C) 2021 Fondazione Bruno Kessler.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Alberto Debiasi - initial API and implementation
 ******************************************************************************/
package org.polarsys.chess.customEditors.editors.cleanc;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.textedit.property.xtext.ui.contributions.PropertyXtextDirectEditorConfiguration;
import org.eclipse.papyrus.uml.xtext.integration.DefaultXtextDirectEditorConfiguration;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.OpaqueExpression;

import com.google.inject.Injector;

import eu.fbk.tools.editor.c.expression.ui.internal.ExpressionActivator;
import eu.fbk.tools.editor.c.statement.ui.internal.StatementActivator;


public class CleanCEditor extends org.polarsys.chess.customEditors.editors.XtextLanguageEditor{
	
	@Override
	protected DefaultXtextDirectEditorConfiguration getConfigurationFromSelection() {
		EObject semanticElement = getEObject();
		//String semanticClassName = semanticElement.eClass().getInstanceClassName();
		//IDirectEditorConfiguration configuration = DirectEditorsUtil.findEditorConfiguration(languagePreferred,
		//		semanticClassName);
		if (semanticElement != null) {
			if(semanticElement instanceof OpaqueExpression){
				DefaultXtextDirectEditorConfiguration xtextConfiguration = new CleanCExpressionEditorConfiguration();
				xtextConfiguration.setLanguage("CleanC");
				xtextConfiguration.preEditAction(semanticElement);
				return xtextConfiguration;			
			}else if(semanticElement instanceof OpaqueBehavior){
				DefaultXtextDirectEditorConfiguration xtextConfiguration = new CleanCStatementEditorConfiguration();
				xtextConfiguration.setLanguage("CleanC");
				xtextConfiguration.preEditAction(semanticElement);
				return xtextConfiguration;		
			}
		}
		return null;
	}

	
	
}

class CleanCExpressionEditorConfiguration extends PropertyXtextDirectEditorConfiguration {

	@Override
	public Injector getInjector() {
		System.out.println("PropertyXtextDirectEditorConfiguration.getInjector");
		return ExpressionActivator.getInstance()
				.getInjector(ExpressionActivator.EU_FBK_TOOLS_EDITOR_C_EXPRESSION_EXPRESSION);
	}
	
	@Override
	public String getLanguage() {		
		return "CleanC";
	}

}

class CleanCStatementEditorConfiguration extends PropertyXtextDirectEditorConfiguration {

	@Override
	public Injector getInjector() {
		System.out.println("PropertyXtextDirectEditorConfiguration.getInjector");
		return StatementActivator.getInstance()
				.getInjector(StatementActivator.EU_FBK_TOOLS_EDITOR_C_STATEMENT_STATEMENT);
	}

	@Override
	public String getLanguage() {		
		return "CleanC";
	}
	
	

}
