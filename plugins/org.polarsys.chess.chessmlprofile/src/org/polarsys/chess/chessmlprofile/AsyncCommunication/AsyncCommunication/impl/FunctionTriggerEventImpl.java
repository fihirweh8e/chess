/**
 */
package org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.papyrus.sysml.blocks.Block;

import org.eclipse.uml2.uml.Event;

import org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.AsyncCommunicationPackage;
import org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Trigger Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.FunctionTriggerEventImpl#getTriggeredBlock <em>Triggered Block</em>}</li>
 *   <li>{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.FunctionTriggerEventImpl#getBase_Event <em>Base Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionTriggerEventImpl extends EObjectImpl implements FunctionTriggerEvent {
	/**
	 * The cached value of the '{@link #getTriggeredBlock() <em>Triggered Block</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggeredBlock()
	 * @generated
	 * @ordered
	 */
	protected Block triggeredBlock;

	/**
	 * The cached value of the '{@link #getBase_Event() <em>Base Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Event()
	 * @generated
	 * @ordered
	 */
	protected Event base_Event;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionTriggerEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AsyncCommunicationPackage.Literals.FUNCTION_TRIGGER_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block getTriggeredBlock() {
		if (triggeredBlock != null && triggeredBlock.eIsProxy()) {
			InternalEObject oldTriggeredBlock = (InternalEObject)triggeredBlock;
			triggeredBlock = (Block)eResolveProxy(oldTriggeredBlock);
			if (triggeredBlock != oldTriggeredBlock) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__TRIGGERED_BLOCK, oldTriggeredBlock, triggeredBlock));
			}
		}
		return triggeredBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Block basicGetTriggeredBlock() {
		return triggeredBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTriggeredBlock(Block newTriggeredBlock) {
		Block oldTriggeredBlock = triggeredBlock;
		triggeredBlock = newTriggeredBlock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__TRIGGERED_BLOCK, oldTriggeredBlock, triggeredBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event getBase_Event() {
		if (base_Event != null && base_Event.eIsProxy()) {
			InternalEObject oldBase_Event = (InternalEObject)base_Event;
			base_Event = (Event)eResolveProxy(oldBase_Event);
			if (base_Event != oldBase_Event) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__BASE_EVENT, oldBase_Event, base_Event));
			}
		}
		return base_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event basicGetBase_Event() {
		return base_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Event(Event newBase_Event) {
		Event oldBase_Event = base_Event;
		base_Event = newBase_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__BASE_EVENT, oldBase_Event, base_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__TRIGGERED_BLOCK:
				if (resolve) return getTriggeredBlock();
				return basicGetTriggeredBlock();
			case AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__BASE_EVENT:
				if (resolve) return getBase_Event();
				return basicGetBase_Event();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__TRIGGERED_BLOCK:
				setTriggeredBlock((Block)newValue);
				return;
			case AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__BASE_EVENT:
				setBase_Event((Event)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__TRIGGERED_BLOCK:
				setTriggeredBlock((Block)null);
				return;
			case AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__BASE_EVENT:
				setBase_Event((Event)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__TRIGGERED_BLOCK:
				return triggeredBlock != null;
			case AsyncCommunicationPackage.FUNCTION_TRIGGER_EVENT__BASE_EVENT:
				return base_Event != null;
		}
		return super.eIsSet(featureID);
	}

} //FunctionTriggerEventImpl
