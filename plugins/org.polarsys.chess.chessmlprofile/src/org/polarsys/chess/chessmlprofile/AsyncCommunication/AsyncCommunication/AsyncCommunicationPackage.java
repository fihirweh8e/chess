/**
 */
package org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.AsyncCommunicationFactory
 * @model kind="package"
 * @generated
 */
public interface AsyncCommunicationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "AsyncCommunication";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://CHESS/AsyncCommunication";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "AsyncCommunication";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AsyncCommunicationPackage eINSTANCE = org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.AsyncCommunicationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.FunctionTriggerEventImpl <em>Function Trigger Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.FunctionTriggerEventImpl
	 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.AsyncCommunicationPackageImpl#getFunctionTriggerEvent()
	 * @generated
	 */
	int FUNCTION_TRIGGER_EVENT = 0;

	/**
	 * The feature id for the '<em><b>Triggered Block</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TRIGGER_EVENT__TRIGGERED_BLOCK = 0;

	/**
	 * The feature id for the '<em><b>Base Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TRIGGER_EVENT__BASE_EVENT = 1;

	/**
	 * The number of structural features of the '<em>Function Trigger Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TRIGGER_EVENT_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent <em>Function Trigger Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Trigger Event</em>'.
	 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent
	 * @generated
	 */
	EClass getFunctionTriggerEvent();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent#getTriggeredBlock <em>Triggered Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Triggered Block</em>'.
	 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent#getTriggeredBlock()
	 * @see #getFunctionTriggerEvent()
	 * @generated
	 */
	EReference getFunctionTriggerEvent_TriggeredBlock();

	/**
	 * Returns the meta object for the reference '{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent#getBase_Event <em>Base Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Event</em>'.
	 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.FunctionTriggerEvent#getBase_Event()
	 * @see #getFunctionTriggerEvent()
	 * @generated
	 */
	EReference getFunctionTriggerEvent_Base_Event();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AsyncCommunicationFactory getAsyncCommunicationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.FunctionTriggerEventImpl <em>Function Trigger Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.FunctionTriggerEventImpl
		 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.AsyncCommunicationPackageImpl#getFunctionTriggerEvent()
		 * @generated
		 */
		EClass FUNCTION_TRIGGER_EVENT = eINSTANCE.getFunctionTriggerEvent();

		/**
		 * The meta object literal for the '<em><b>Triggered Block</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_TRIGGER_EVENT__TRIGGERED_BLOCK = eINSTANCE.getFunctionTriggerEvent_TriggeredBlock();

		/**
		 * The meta object literal for the '<em><b>Base Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_TRIGGER_EVENT__BASE_EVENT = eINSTANCE.getFunctionTriggerEvent_Base_Event();

	}

} //AsyncCommunicationPackage
