/**
 */
package org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.AsyncCommunicationPackage
 * @generated
 */
public interface AsyncCommunicationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AsyncCommunicationFactory eINSTANCE = org.polarsys.chess.chessmlprofile.AsyncCommunication.AsyncCommunication.impl.AsyncCommunicationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Function Trigger Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Trigger Event</em>'.
	 * @generated
	 */
	FunctionTriggerEvent createFunctionTriggerEvent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AsyncCommunicationPackage getAsyncCommunicationPackage();

} //AsyncCommunicationFactory
